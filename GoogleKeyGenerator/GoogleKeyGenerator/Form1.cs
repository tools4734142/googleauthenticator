﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Cloud.Storage.V1;

namespace GoogleKeyGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string GenerateEncryptionKey()
        {
            var encryptionKey = EncryptionKey.Generate().Base64Key;
            Console.WriteLine($"Generated Base64-encoded AES-256 encryption key: {encryptionKey}");
            return encryptionKey;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText( GenerateEncryptionKey() +"\r\n");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "谷歌秘钥生成器 ver2023.4.5";
            button1.Text = "创建";
        }
    }
}
